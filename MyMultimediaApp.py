#pick dia bibliothèque ahafahan manamboatra menu interactif miala @ basique
from pick import pick
import random

#Jeu de dés
def diceGame() :
    title = 'Tongasoa eto @ lalao kodia kely!'
    options = ['Tsindrio eto raha hanomboka ny lalao', 'Tsindrio eto raha hiala']
    option, index = pick(options, title)
    if index == 0 :
        choice = random.randint(1,6)
        print(choice)
    elif index == 1:
        return None
    

#Jeu de guess number
def guessNumber():
    choice =  input("Misafidiana isa ao anatin'ny 1 hatr@ 10 : ")
    randomNumber = random.randint(1,2)
    for i in range(2,4):
        if choice == randomNumber:
            print("Tsara fitsatoka ianao!")
            break
        elif choice != randomNumber:
            choice = input(f"Diso, andrana faha-{i} : ")


def play_pendu():
    def sary_miantona(isa_tavela):
        antanatohatra = [
            """
                ------
                |    |
                |
                |
                |
                |
            ----------
            """,
            """
                ------
                |    |
                |    O
                |
                |
                |
            ----------
            """,
            """
                ------
                |    |
                |    O
                |    |
                |
                |
            ----------
            """,
            """
                ------
                |    |
                |    O
                |   /|
                |
                |
            ----------
            """,
            """
                ------
                |    |
                |    O
                |   /|\\
                |   /
                |
            ----------
            """,
            """
                ------
                |    |
                |    O
                |   /|\\
                |   / \\
                |
            ----------
            """
        ]
        return antanatohatra[isa_tavela]

    lisitra_teny = ["OLONA", "MARAINA", "FITAFY", "BIBILAVA", "LATABATRA"]
    safidy = None

    while safidy != "0":
        print('''

        Safidio izay tianao:

        0 - Hiala
        1 - Hampiditra teny vaovao anaty lisitra
        2 - Hilalao

        ''')
        safidy = input("Ampidiro ny safidinao: ")

        if safidy == "0":
            print("Eo andalam-pialana....")
        elif safidy == "1":
            lisitra_teny = []
            x = 1
            while x != 5:
                teny = input(f"Ampidiro ny teny vaovao laharana {x}: ")
                lisitra_teny.append(teny.upper())
                x += 1
        elif safidy == "2":
            teny_antsapaka = random.choice(lisitra_teny).upper()
            teny_miafina = "_" * len(teny_antsapaka)
            isa_tavela = 6
            tsapaka = []

            while isa_tavela > 0 and teny_miafina != teny_antsapaka:
                print("\n*****************************")
                print("Ilay teny dia")
                print(teny_miafina)
                print("\n Misy", len(teny_antsapaka), "ny litera ao @ ilay teny")
                print("Ny litera nampidirinao dia: ")
                print(' '.join(tsapaka))
                print(sary_miantona(6 - isa_tavela))
                print("\n Manana andrana ", isa_tavela, " sisa ianao")
                litera_tsapaka = input("\nMampidira litera: ").upper()

                if len(litera_tsapaka) != 1 or not litera_tsapaka.isalpha():
                    print("\nDiso ny nampidirinao! mampidira litera iray(1)")
                    continue
                
                if litera_tsapaka in tsapaka:
                    print("\nefa nampidirinao io litera io!")
                    continue

                tsapaka.append(litera_tsapaka) 

                if litera_tsapaka in teny_antsapaka:
                    print("*******************************")
                    print("Mahafinaritra!", litera_tsapaka, "dia ao anatin'ilay teny")
                    ilay_teny = ""
                    for i in range(len(teny_antsapaka)):
                        if litera_tsapaka == teny_antsapaka[i]:
                            ilay_teny += litera_tsapaka
                        else:
                            ilay_teny += teny_miafina[i]
                    teny_miafina = ilay_teny
                else:
                    print("**************************")
                    print("Miala tsiny, ", litera_tsapaka, "tsy ao anaty teny")
                    isa_tavela -= 1

            if isa_tavela == 0:
                print("RESY ENAO! tsy manana isa tavela intsony")
                print("Ilay teny dia: ", teny_antsapaka)
            else:
                print("\nARAHABAINA! hitanao ilay teny miafina")
                print("Ilay teny dia: ", teny_antsapaka)
                print("\nMisaotra anao nilalao")
        else:
            print("\nDiso ny nampidirinao! mamerena mampiditra azafady!")


    
while True:

    title = 'Salama e! Tongasoa, tsindrio ny safidinao : '
    options = ['1.Hilalao kodia kely', '2.Hilalao isa tsatoka', '3.Miantona', '4.Hiala']
    option, index = pick(options, title)

    if index == 0:
        diceGame()
    elif index == 1:
        guessNumber()
    elif index == 2:
        play_pendu()
    elif index == 3:
        break

    
